namespace Lessons.StateMachines.States
{
    public enum CharacterStateType
    {
        Idle,
        Run,
        Dead,
        Gathering
    }
}