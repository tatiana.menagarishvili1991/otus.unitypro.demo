namespace Lessons.Architecture.Components
{
    public interface ITakeDamageComponent
    {
        void TakeDamage(int damage);
    }
}